#include <iostream>
#include "Math.h"
#include "Helpers.h"

#define PI 3.14
#define LOG

using namespace std;

int main()
{
#ifdef LOG
	cout << "log massage" << endl;
#endif

	const int x = 10;
	const int y = 20;

	cout << "Result = " << SquaredAmount(x, y) << endl << PI << endl;
	return 0;
}